# Bolter App
Este projeto consiste em um aplicativo voltado para investidores da BOVESPA interessados em estratégias de longo prazo. Nele o usuário poderá definir e cadastrar estratégias de investimentos com base nos indicadores fundamentalistas.

## TODO
 - Exception Handling nas calls para a API
 - Adicionar a opção de salvar os filtros
 - Estilizar a página da empresa
 - montar os graficos 
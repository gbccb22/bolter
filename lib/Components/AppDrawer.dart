import 'package:flutter/material.dart';

class AppDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
        child: Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: <Widget>[
          DrawerHeader(
            decoration: BoxDecoration(color: Theme.of(context).primaryColor),
            child: Text('Menu de opções'),
          ),
          ListTile(
            onTap: () {
              Navigator.of(context).pop();
            },
            leading: Text('Lista de ativos', style: Theme.of(context).textTheme.subhead,),
            trailing: Icon(Icons.arrow_forward),
          ),
        ],
      ),
    ));
  }
}

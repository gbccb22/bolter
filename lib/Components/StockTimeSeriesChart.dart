import 'package:bolter_app/bolterAPI/BolterAPI.dart';

/// Timeseries chart example
import 'package:charts_flutter/flutter.dart' as charts;
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class SimpleTimeSeriesChart extends StatelessWidget {
  final List<charts.Series> seriesList;
  final bool animate;
  final String keyValue;
  SimpleTimeSeriesChart(this.seriesList, this.keyValue, {this.animate});

  @override
  Widget build(BuildContext context) {
    return new charts.TimeSeriesChart(seriesList,
        animate: animate,
        // Optionally pass in a [DateTimeFactory] used by the chart. The factory
        // should create the same type of [DateTime] as the data provided. If none
        // specified, the default creates local date time.
        dateTimeFactory: const charts.LocalDateTimeFactory(),
        behaviors: [
          charts.ChartTitle(keyValue),
        ]);
  }

  /// Create one series with sample hard coded data.
  static Future<List<charts.Series<TimeSeriesSales, DateTime>>>
      createSampleData(String ticker) async {
    List<TimeSeriesSales> data = [];
    final response = await http.get(
        'https://statusinvest.com.br/category/tickerprice?ticker=$ticker&type=4');
    if (response.statusCode == 200) {
      final parsedJson = jsonDecode(response.body)['prices'].toList();
      parsedJson.forEach((share) {
        data.add(TimeSeriesSales.fromJson(share));
      });
    }
    return [
      new charts.Series<TimeSeriesSales, DateTime>(
        id: 'Sales',
        colorFn: (_, __) => charts.MaterialPalette.black,
        domainFn: (TimeSeriesSales sales, _) => sales.time,
        measureFn: (TimeSeriesSales sales, _) => sales.sales,
        data: data,
      )
    ];
  }

  static Future<List<charts.Series<TimeSeriesSales, DateTime>>>
      fundamanetalistData(
          String companyName, String ticker, String keyValue) async {
    List<TimeSeriesSales> data = [];
    final response =
        await BolterAPI.getFundamentalistData(companyName, ticker, keyValue);
    response.forEach((element) {
      data.add(TimeSeriesSales.fromFundamentalistJson(element, keyValue));
    });
    return [
      new charts.Series<TimeSeriesSales, DateTime>(
        id: 'Sales',
        colorFn: (_, __) => charts.MaterialPalette.black,
        domainFn: (TimeSeriesSales sales, _) => sales.time,
        measureFn: (TimeSeriesSales sales, _) => sales.sales,
        data: data,
      )
    ];
  }
}

/// Sample time series data type.
class TimeSeriesSales {
  final DateTime time;
  final double sales;

  TimeSeriesSales({this.time, this.sales});

  factory TimeSeriesSales.fromJson(Map<String, dynamic> json) {
    String dataFull = json['date'];
    String data = dataFull.split(' ')[0];
    String hora = dataFull.split(' ')[1];
    print(data);
    print(hora);

    int dia = int.parse(data.split('/')[0]);
    int mes = int.parse(data.split('/')[1]);
    int ano = int.parse(data.split('/')[2]) + 2000;
    print(dia);
    print(mes);
    print(ano);
    return TimeSeriesSales(
        time: DateTime.utc(ano, mes, dia), sales: json['price']);
  }

  factory TimeSeriesSales.fromFundamentalistJson(
      Map<String, dynamic> json, String keyValue) {
    return TimeSeriesSales(
        time: DateTime.utc(json['year'], 1, 1), sales: json[keyValue]);
  }
}

import 'dart:async';
import 'package:bolter_app/bolterAPI/BolterAPI.dart';
import 'package:http/http.dart' as http;
import 'package:bolter_app/tools/Ativo.dart';

class ShareBloc{
  int _shares;
  int get shares => _shares;

  final shareController = StreamController<List<Ativo>>.broadcast();
  Stream<List<Ativo>> get shareStream => shareController.stream;

  Future<List<Ativo>> getShares() async{
    shareController.sink.add(await BolterAPI.fetchListaDeAtivos(http.Client()));
  }

 void dispose(){
   shareController.close();
 }
}

ShareBloc shareBloc = ShareBloc();
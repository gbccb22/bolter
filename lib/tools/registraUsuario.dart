import 'package:http/http.dart' as http;

Future<Map<String, dynamic>> registerUser(email, username, password1, password2) async{
  var url = 'https://bolter-api.herokuapp.com/api/v1/users/registration';
  var response = await http.post(url, body:{'email': email, 'username':username, 'password': password1, 'password2': password2});
  print('Response status: ${response.statusCode}');
  print('Response body: ${response.body}');

  return {
    'response': response.body,
    'status': response.statusCode
  };
}
class User{
  String username = 'nome';
  String email = 'abcd@exemplo.com';

  User({this.username, this.email});

  factory User.fromJson(Map<String, dynamic> json){
    return User(
      username: json["username"],
      email: json["email"],
    );
  }

}
class Ativo {
  final int id;
  final int companyId;
  final String companyName;
  final double dy;
  final double liquidezCorrente;
  final double lucros_Cagr5;
  final double margemBruta;
  final double margemEbit;
  final double margemLiquida;
  final double p_ativo;
  final double p_ativoCirculante;
  final double p_capitalGiro;
  final double p_L;
  final double p_SR;
  final double p_VP;
  final double passivo_Ativo;
  final double pl_Ativo;
  final double price;
  final double receitas_Cagr5;
  final double roa;
  final double roe;
  final String ticker;

  Ativo(
      {this.id,
      this.companyId,
      this.companyName,
      this.dy,
      this.liquidezCorrente,
      this.lucros_Cagr5,
      this.margemBruta,
      this.margemEbit,
      this.margemLiquida,
      this.p_ativo,
      this.p_ativoCirculante,
      this.p_capitalGiro,
      this.p_L,
      this.p_SR,
      this.p_VP,
      this.passivo_Ativo,
      this.pl_Ativo,
      this.price,
      this.receitas_Cagr5,
      this.roa,
      this.roe,
      this.ticker});

  factory Ativo.fromJson(Map<String, dynamic> json) {
    double nullValue = 0;
    return Ativo(
        id: json['id'],
        companyId: json['companyId'].round(),
        companyName: json['companyName'],
        dy: json['dy'] == null ? 0 : json['dy'],
        liquidezCorrente:
            json['liquidezCorrente'] == null ? 0 : json['liquidezCorrente'],
        lucros_Cagr5: json['lucros_Cagr5'] == null ? 0 : json['lucros_Cagr5'],
        margemBruta: json['margemBruta'] == null ? 0 : json['margemBruta'],
        margemEbit: json['margemEbit'] == null ? -0 : json['margemEbit'],
        margemLiquida:
            json['margemLiquida'] == null ? 0 : json['margemLiquida'],
        p_ativo: json['p_ativo'] == null ? 0 : json['p_ativo'],
        p_ativoCirculante:
            json['p_ativoCirculante'] == null ? 0 : json['p_ativoCirculante'],
        p_capitalGiro:
            json['p_capitalGiro'] == null ? 0 : json['p_capitalGiro'],
        p_L: json['p_L'] == null ? 0 : json['p_L'],
        p_SR: json['p_SR'] == null ? 0 : json['p_SR'],
        p_VP: json['p_VP'] == null ? 0 : json['p_VP'],
        passivo_Ativo:
            json['passivo_Ativo'] == null ? 0 : json['passivo_Ativo'],
        pl_Ativo: json['pl_ativo'] == null ? 0 : json['pl_ativo'],
        price: json['price'],
        receitas_Cagr5:
            json['receitas_Cagr5'] == null ? 0 : json['receitas_Cagr5'],
        roa: json['roa'] == null ? 0 : json['roa'],
        roe: json['roe'] == null ? 0 : json['roe'],
        ticker: json['ticker']);
  }

  @override
  String toString() {
    return '''nome: ${this.companyName}
dy: ${this.dy}
Liquidez Corrente: ${this.liquidezCorrente}
Lucros CAGR5: ${this.lucros_Cagr5}
Margem Bruta: ${this.margemBruta}
Margem EBIT: ${this.margemEbit}
Margem Líquida: ${this.margemLiquida}
p_ativo: ${this.p_ativo},
p_ativoCirculante: ${this.p_ativoCirculante},
p_capitalGiro: ${this.p_capitalGiro},
p_L: ${this.p_L},
p_SR: ${this.p_SR}
p_VP: ${this.p_VP}
passivo_Ativo: ${this.passivo_Ativo}
pl_Ativo: ${this.pl_Ativo}
price: ${this.price}
receitas_Cagr5: ${this.receitas_Cagr5}
roa: ${this.roa}
roe: ${this.roe}
ticker: ${this.ticker}''';
  }
}

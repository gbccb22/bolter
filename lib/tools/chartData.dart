import 'dart:async';
import 'package:http/http.dart' as http;
import 'dart:convert';

void main(List<String> args) {
  getChartData('PETRO4');
}

Future<List<List<dynamic>>> getChartData(String nomeDoAtivo) async {
  List<List<dynamic>> listaRetorno;
  String url = "http://www.fundamentus.com.br/amline/cot_hist.php?papel=$nomeDoAtivo";
  var stringData = await http.get(url);
  listaRetorno = List<List<dynamic>>.from(jsonDecode(stringData.body));
  print(listaRetorno);
  print(listaRetorno.length);
  return listaRetorno;
}

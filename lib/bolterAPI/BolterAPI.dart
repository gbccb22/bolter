import 'dart:convert';
import 'package:bolter_app/tools/Ativo.dart';
import 'package:bolter_app/tools/ShareBloc.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:path_provider/path_provider.dart' as path_provider;
import 'dart:io' as io;
import 'package:http/http.dart' as http;

class BolterAPI {
  static Future<String> get _localPath async {
    WidgetsFlutterBinding.ensureInitialized();
    final directory = await path_provider.getTemporaryDirectory();
    return directory.path;
  }

  static Future<io.File> get _localFileKey async {
    final path = await _localPath;
    print(path);
    return io.File('$path/key.txt');
  }

  static Future<io.File> writeKey(String key) async {
    final file = await _localFileKey;
    return file.writeAsString(key);
  }

  static Future<void> deleteFileLogout() async {
    final path = await _localPath;
    final file = io.File('$path/key.txt');
    if (await file.exists()) {
      await file.delete();
    }
  }

  static Future<List<Ativo>> parseAtivo(String responseBody) async {
    final parsed = json.decode(responseBody).cast<Map<String, dynamic>>();
    print("Dados provenientes do servidor");
    var allShares = parsed.map<Ativo>((json) => Ativo.fromJson(json)).toList();
    shareBloc.shareController.sink.add(allShares);
    return allShares;
  }

  static Future<List<Ativo>> parseFileData(String contents) {
    final parsed = json.decode(contents).cast<Map<String, dynamic>>();
    return parsed.map<Ativo>((json) => Ativo.fromJson(json)).toList();
  }

  static Future<List<Ativo>> fetchListaDeAtivos(http.Client client) async {
    var url = 'https://bolter-api.herokuapp.com/api/v1/ativos/listaDeAtivos';
    var response = await client.get(url);
    return compute(parseAtivo, response.body);
  }

  static Future<bool> register(
      String username, String email, String password1, String password2) async {
    var url = 'https://bolter-api.herokuapp.com/api/v1/users/registration';

    Map params = {
      "username": username,
      "email": email,
      "password": password1,
      "password2": password2
    };

    var response = await http.post(url, body: params);
    Map mapResponse = json.decode(response.body);

    print('$mapResponse');
    print('${response.statusCode}');

    if (response.statusCode == 201) {
      writeKey(mapResponse["key"]);
      return true;
    } else {
      return false;
    }
  }

  static Future<bool> login(String email, String password) async {
    var url = 'https://bolter-api.herokuapp.com/api/v1/users/login/';

    Map params = {'username': "", 'email': email, 'password': password};

    var response = await http.post(url, body: params);

    Map mapResponse = json.decode(response.body);

    print("$mapResponse");
    print("${response.statusCode}");

    if (response.statusCode == 200) {
      writeKey(mapResponse["key"]);
      return true;
    } else {
      return false;
    }
  }

  static Future<List<Map<String, dynamic>>> getFundamentalistData(
      String companyName, String ticker, String keyValue) async {
    List<Map<String, dynamic>> data = [];
    String normalizedName;
    final responseNormalizedName = await http
        .get('https://statusinvest.com.br/home/mainsearchquery?q=$ticker');

    List<Map<String, dynamic>> listNormalizedName =
        List<Map<String, dynamic>>.from(
            jsonDecode(responseNormalizedName.body));

    if (responseNormalizedName.statusCode == 200) {
      listNormalizedName.forEach((element) {
        if (element['code'] == ticker) {
          normalizedName = element['normalizedName'];
        }
      });
    }

    String url1 =
        'https://statusinvest.com.br/acao/getrevenue?companyName=$normalizedName&type=2';
    String url2 =
        'https://statusinvest.com.br/acao/getbsactivepassivechart?companyName=$normalizedName&type=1';

    final response1 = await http.get(url1);
    final response2 = await http.get(url2);

    if (response1.statusCode == 200 && response2.statusCode == 200) {
      final parsedJson1 =
          List<Map<String, dynamic>>.from(jsonDecode(response1.body)).toList();
      final parsedJson2 =
          List<Map<String, dynamic>>.from(jsonDecode(response2.body)).toList();

      if ([
        "receitaLiquida",
        "despesas",
        "lucroLiquido",
        "margemBruta",
        "margemEbitda",
        "margemEBit",
        "margemLiquida"
      ].contains(keyValue)) {
        data = parsedJson1;
      } else {
        data = parsedJson2;
      }
      print(data);
      return data;
    }
    return null;
  }
}

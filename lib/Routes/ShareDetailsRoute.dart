import 'package:async_loader/async_loader.dart';
import 'package:bolter_app/tools/Ativo.dart';
import 'package:flutter/material.dart';
import 'package:bolter_app/Components/StockTimeSeriesChart.dart';

class ShareDetalisRoute extends StatelessWidget {
  final GlobalKey<AsyncLoaderState> asyncLoaderState =
      new GlobalKey<AsyncLoaderState>();
  final GlobalKey<AsyncLoaderState> asyncLoaderStateMargemLiquida =
      new GlobalKey<AsyncLoaderState>();
  final GlobalKey<AsyncLoaderState> asyncLoaderStateReceitaLiquida =
      new GlobalKey<AsyncLoaderState>();
  final GlobalKey<AsyncLoaderState> asyncLoaderStateMargemBruta =
      new GlobalKey<AsyncLoaderState>();
  final GlobalKey<AsyncLoaderState> asyncLoaderStateAtivoTotal =
      new GlobalKey<AsyncLoaderState>();

  @override
  Widget build(BuildContext context) {
    final Ativo share = ModalRoute.of(context).settings.arguments;
    var _asyncLoader = new AsyncLoader(
      key: asyncLoaderState,
      initState: () async =>
          await SimpleTimeSeriesChart.createSampleData(share.ticker),
      renderLoad: () => Center(child: new CircularProgressIndicator()),
      renderError: ([error]) => Center(
        child: Text(
            "Erro na contrução do gráfico. Verifique a conexão com a internet."),
      ),
      renderSuccess: ({data}) =>
          SimpleTimeSeriesChart(data, "Cotação", animate: false),
    );

    var _asyncLoaderMargemLiquida = AsyncLoader(
      key: asyncLoaderStateMargemLiquida,
      initState: () async => await SimpleTimeSeriesChart.fundamanetalistData(
          share.companyName, share.ticker, 'margemLiquida'),
      renderLoad: () => Center(
        child: CircularProgressIndicator(),
      ),
      renderError: ([error]) => Center(
        child: Text(
            "Erro na contrução do gráfico. Verifique a conexão com a internet."),
      ),
      renderSuccess: ({data}) => SimpleTimeSeriesChart(
        data,
        "Margem Líquida",
        animate: false,
      ),
    );

    var _asyncLoaderReceitaLiquida = AsyncLoader(
      key: asyncLoaderStateReceitaLiquida,
      initState: () async => await SimpleTimeSeriesChart.fundamanetalistData(
          share.companyName, share.ticker, 'receitaLiquida'),
      renderLoad: () => Center(
        child: CircularProgressIndicator(),
      ),
      renderError: ([error]) => Center(
        child: Text(
            "Erro na contrução do gráfico. Verifique a conexão com a internet."),
      ),
      renderSuccess: ({data}) => SimpleTimeSeriesChart(
        data,
        "Receita Líquida",
        animate: false,
      ),
    );

    var _asyncLoaderMargemBruta = AsyncLoader(
      key: asyncLoaderStateMargemBruta,
      initState: () async => await SimpleTimeSeriesChart.fundamanetalistData(
          share.companyName, share.ticker, 'margemBruta'),
      renderLoad: () => Center(
        child: CircularProgressIndicator(),
      ),
      renderError: ([error]) => Center(
        child: Text(
            "Erro na contrução do gráfico. Verifique a conexão com a internet."),
      ),
      renderSuccess: ({data}) => SimpleTimeSeriesChart(
        data,
        "Margem Bruta",
        animate: false,
      ),
    );

    var _asyncLoaderAtivoTotal = AsyncLoader(
      key: asyncLoaderStateAtivoTotal,
      initState: () async => await SimpleTimeSeriesChart.fundamanetalistData(
          share.companyName, share.ticker, 'ativoTotal'),
      renderLoad: () => Center(
        child: CircularProgressIndicator(),
      ),
      renderError: ([error]) => Center(
        child: Text(
            "Erro na contrução do gráfico. Verifique a conexão com a internet."),
      ),
      renderSuccess: ({data}) => SimpleTimeSeriesChart(
        data,
        "Ativo Total",
        animate: false,
      ),
    );

    return Scaffold(
      appBar: AppBar(
        title: Text("${share.ticker}"),
      ),
      body: Center(
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  SizedBox(
                    height: 20,
                  ),
                  Container(
                    decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        image: DecorationImage(
                            image: NetworkImage(
                                'https://cdn-statusinvest.azureedge.net/img/company/cover/${share.companyId.toString()}.jpg'),
                            fit: BoxFit.cover)),
                    height: 100,
                    width: 100,
                  ),
                  Container(
                    margin: EdgeInsets.only(bottom: 20, top: 5),
                    child: Text(
                      "${share.companyName}",
                      style: TextStyle(
                        fontSize: 20,
                      ),
                    ),
                  ),
                ],
              ),
              Container(
                child: _asyncLoader,
                width: 400,
                height: 400,
              ),
              SizedBox(
                height: 40,
              ),
              Container(
                child: _asyncLoaderMargemLiquida,
                width: 400,
                height: 400,
              ),
              SizedBox(
                height: 40,
              ),
              Container(
                child: _asyncLoaderReceitaLiquida,
                width: 400,
                height: 400,
              ),
              SizedBox(
                height: 40,
              ),
              Container(
                child: _asyncLoaderMargemBruta,
                height: 400,
                width: 400,
              ),
              SizedBox(
                height: 40,
              ),
              Container(
                child: _asyncLoaderAtivoTotal,
                height: 400,
                width: 400,
              ),
              SizedBox(
                height: 40,
              ),
              Column(
                children: [
                  Text(
                    "DY: ${share.dy}",
                    style: TextStyle(fontSize: 20),
                  ),
                  Text(
                    "ROA: ${share.roa}",
                    style: TextStyle(fontSize: 20),
                  ),
                  Text(
                    "ROE: ${share.roe}",
                    style: TextStyle(fontSize: 20),
                  ),
                  Text(
                    "Margem EBIT: ${share.margemEbit}",
                    style: TextStyle(fontSize: 20),
                  ),
                  Text(
                    "Passivo/Ativo: ${share.passivo_Ativo}",
                    style: TextStyle(fontSize: 20),
                  )
                ],
              ),
              SizedBox(
                height: 40,
              )
            ],
          ),
        ),
      ),
    );
  }
}

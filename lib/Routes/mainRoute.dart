import 'package:backdrop/backdrop.dart';
import 'package:bolter_app/tools/Ativo.dart';
import 'package:bolter_app/tools/ShareBloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_masked_text/flutter_masked_text.dart';
import 'package:bolter_app/bolterAPI/BolterAPI.dart';

class MainRoute extends StatefulWidget {
  @override
  _MainRouteState createState() => _MainRouteState();
}

class _MainRouteState extends State<MainRoute> {
  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  List<Ativo> totalShares;
  List<Ativo> totalSharesCopy;
  int contador = 1;
  GlobalKey streamListGlobalkey = GlobalKey();
  bool isFilterEmptyList = false;

  void searchShare(String searchQuery) {
    if (contador == 1) {
      totalSharesCopy = totalShares;
      contador = 0;
    }
    print('amount of orinal shares: ${totalSharesCopy.length}');
    List<Ativo> searchResult = [];
    shareBloc.shareController.sink.add(null);

    if (searchQuery.isEmpty) {
      print("total shares length: ${totalShares.length}");
      shareBloc.shareController.sink.add(totalSharesCopy);
      return;
    }

    totalSharesCopy.forEach((share) {
      print("the search query is: $searchQuery");
      if (share.ticker.toLowerCase().contains(searchQuery.toLowerCase()) ==
          true) {
        searchResult.add(share);
      }
    });

    print('Search Result Length: ${searchResult.length}');
    shareBloc.shareController.sink.add(searchResult);
  }

  Widget shareListItem(Ativo share, BuildContext context) {
    return ListTile(
      onTap: () {
        FocusScope.of(context).unfocus();
        Navigator.of(context).pushNamed('/shareDetailsRoute', arguments: share);
      },
      leading: Icon(Icons.monetization_on),
      title: Text(share.ticker),
      trailing: Text('\$${share.price}'.replaceAll('.', ',')),
    );
  }

  Widget shareList() {
    return StreamBuilder(
        key: streamListGlobalkey,
        stream: shareBloc.shareController.stream,
        builder: (BuildContext context, AsyncSnapshot<List<Ativo>> snapshot) {
          if (!(snapshot.data == null)) {
            if (snapshot.data.length == 0) {
              return Center(child: Text("Sem ações correspondentes"));
            }
          }
          if (snapshot.data == null) {
            shareBloc.getShares();
            return Center(child: CircularProgressIndicator());
          }
          return snapshot.connectionState == ConnectionState.waiting
              ? Center(child: CircularProgressIndicator())
              : ListView.builder(
                  itemCount: snapshot.data.length,
                  itemBuilder: (BuildContext context, int index) {
                    if (contador == 1) {
                      totalSharesCopy = totalShares;
                      contador = 0;
                    }
                    return shareListItem(snapshot.data[index], context);
                  });
        });
  }

  final myController = TextEditingController();

  Widget searchBox() {
    return Padding(
      padding: const EdgeInsets.all(10.0),
      child: TextField(
        onChanged: (text) => searchShare(text),
        decoration: InputDecoration(
            prefixIcon: Icon(Icons.search),
            hintText: 'Procurar ação',
            contentPadding: EdgeInsets.symmetric(horizontal: 30, vertical: 20),
            border: OutlineInputBorder(
                borderSide: BorderSide(width: 3.1, color: Colors.amber[700]),
                borderRadius: BorderRadius.circular(30))),
      ),
    );
  }

  var controllerMargemLiquidaMin =
      MoneyMaskedTextController(decimalSeparator: ',', thousandSeparator: '.');
  var controllerMargemLiquidaMax =
      MoneyMaskedTextController(decimalSeparator: ',', thousandSeparator: '.');
  var controllerPLMax =
      MoneyMaskedTextController(decimalSeparator: ',', thousandSeparator: '.');
  var controllerPLMin =
      MoneyMaskedTextController(decimalSeparator: ',', thousandSeparator: '.');
  var controllerPassivoAtivoMax =
      MoneyMaskedTextController(decimalSeparator: ',', thousandSeparator: '.');
  var controllerPassivoAtivoMin =
      MoneyMaskedTextController(decimalSeparator: ',', thousandSeparator: '.');
  var controllerDYMin =
      MoneyMaskedTextController(decimalSeparator: ',', thousandSeparator: '.');
  var controllerDYMax =
      MoneyMaskedTextController(decimalSeparator: ',', thousandSeparator: '.');
  var controllerLiquidezCorrenteMin =
      MoneyMaskedTextController(decimalSeparator: ',', thousandSeparator: '.');
  var controllerLiquidezCorrenteMax =
      MoneyMaskedTextController(decimalSeparator: ',', thousandSeparator: '.');
  var controllerLucrosCagr5Min =
      MoneyMaskedTextController(decimalSeparator: ',', thousandSeparator: '.');
  var controllerLucrosCagr5Max =
      MoneyMaskedTextController(decimalSeparator: ',', thousandSeparator: '.');
  var controllerMargemBrutaMax =
      MoneyMaskedTextController(decimalSeparator: ',', thousandSeparator: '.');
  var controllerMargemBrutaMin =
      MoneyMaskedTextController(decimalSeparator: ',', thousandSeparator: '.');
  var controllerMargemEbitMin =
      MoneyMaskedTextController(decimalSeparator: ',', thousandSeparator: '.');
  var controllerMargemEbitMax =
      MoneyMaskedTextController(decimalSeparator: ',', thousandSeparator: '.');
  var controllerPAtivoMin =
      MoneyMaskedTextController(decimalSeparator: ',', thousandSeparator: '.');
  var controllerPAtivoMax =
      MoneyMaskedTextController(decimalSeparator: ',', thousandSeparator: '.');
  var controllerPAtivoCirculanteMin =
      MoneyMaskedTextController(decimalSeparator: ',', thousandSeparator: '.');
  var controllerPAtivoCirculanteMax =
      MoneyMaskedTextController(decimalSeparator: ',', thousandSeparator: '.');
  var controllerPCapitalDeGiroMin =
      MoneyMaskedTextController(decimalSeparator: ',', thousandSeparator: '.');
  var controllerPCapitalDeGiroMax =
      MoneyMaskedTextController(decimalSeparator: ',', thousandSeparator: '.');
  var controllerPSRMin =
      MoneyMaskedTextController(decimalSeparator: ',', thousandSeparator: '.');
  var controllerPSRMax =
      MoneyMaskedTextController(decimalSeparator: ',', thousandSeparator: '.');
  var controllerPVPMin =
      MoneyMaskedTextController(decimalSeparator: ',', thousandSeparator: '.');
  var controllerPVPMax =
      MoneyMaskedTextController(decimalSeparator: ',', thousandSeparator: '.');
  var controllerPLAtivoMin =
      MoneyMaskedTextController(decimalSeparator: ',', thousandSeparator: '.');
  var controllerPLAtivoMax =
      MoneyMaskedTextController(decimalSeparator: ',', thousandSeparator: '.');
  var controllerReceitasCagr5Min =
      MoneyMaskedTextController(decimalSeparator: ',', thousandSeparator: '.');
  var controllerReceitasCagr5Max =
      MoneyMaskedTextController(decimalSeparator: ',', thousandSeparator: '.');
  var controllerROAMin =
      MoneyMaskedTextController(decimalSeparator: ',', thousandSeparator: '.');
  var controllerROAMax =
      MoneyMaskedTextController(decimalSeparator: ',', thousandSeparator: '.');
  var controllerROEMin =
      MoneyMaskedTextController(decimalSeparator: ',', thousandSeparator: '.');
  var controllerROEMax =
      MoneyMaskedTextController(decimalSeparator: ',', thousandSeparator: '.');

  double parseNumber(String number) {
    String string1 = number.trim().replaceAll(",", ".");
    int numberOfDots = '.'.allMatches(string1).length;

    if (numberOfDots == 1) {
      return double.parse(string1);
    } else {
      print(string1);
      int lastDotIndex = string1.lastIndexOf(".");

      String string2 = string1.replaceFirst(".", ",", lastDotIndex);
      String string3 = string2.replaceAll(".", "");
      String result = string3.replaceAll(",", ".");
      return double.parse(result);
    }
  }

  void filterListByProperties() {
    if (contador == 1) {
      totalSharesCopy = totalShares;
      contador = 0;
    }
    List<Ativo> searchResult = [];

    double margemLiquidaMin = controllerMargemLiquidaMin.text == ""
        ? -9999999999
        : parseNumber(controllerMargemLiquidaMin.text);
    double margemLiquidaMax = parseNumber(controllerMargemLiquidaMax.text) == 0
        ? 999999
        : parseNumber(controllerMargemLiquidaMax.text);
    double plMin = controllerPLMin.text == ""
        ? -9999999999
        : parseNumber(controllerPLMin.text);
    double plMax = parseNumber(controllerPLMax.text) == 0
        ? 999999
        : parseNumber(controllerPLMax.text);
    double passivoAtivoMin = controllerPassivoAtivoMin.text == ""
        ? -9999999999
        : parseNumber(controllerPassivoAtivoMin.text);
    double passivoAtivoMax = parseNumber(controllerPassivoAtivoMax.text) == 0
        ? 9999999
        : parseNumber(controllerPassivoAtivoMax.text);
    double dyMin = controllerDYMin.text == ""
        ? -9999999999
        : parseNumber(controllerDYMin.text);
    double dyMax = parseNumber(controllerDYMax.text) == 0
        ? 99999
        : parseNumber(controllerDYMax.text);
    double liquidezCorrenteMin = controllerLiquidezCorrenteMin.text == ""
        ? -9999999999
        : parseNumber(controllerLiquidezCorrenteMin.text);
    double liquidezCorrenteMax =
        parseNumber(controllerLiquidezCorrenteMax.text) == 0
            ? 9999999
            : parseNumber(controllerLiquidezCorrenteMax.text);
    double lucrosCagr5Max = parseNumber(controllerLucrosCagr5Max.text) == 0
        ? 999999999
        : parseNumber(controllerLucrosCagr5Max.text);
    double lucrosCagr5Min = controllerLucrosCagr5Min.text == ""
        ? -9999999999
        : parseNumber(controllerLucrosCagr5Min.text);

    double margemBrutaMin = controllerMargemBrutaMin.text == ""
        ? -9999999999
        : parseNumber(controllerMargemBrutaMin.text);
    double margemBrutaMax = parseNumber(controllerMargemBrutaMax.text) == 0
        ? 999999999
        : parseNumber(controllerMargemBrutaMax.text);

    double margemEbitMin = controllerMargemEbitMin.text == ""
        ? -9999999999
        : parseNumber(controllerMargemEbitMin.text);
    double margemEbitMax = parseNumber(controllerMargemEbitMax.text) == 0
        ? 99999999
        : parseNumber(controllerMargemEbitMax.text);

    double pAtivoMin = controllerPAtivoMin.text == ""
        ? -9999999999
        : parseNumber(controllerPAtivoMin.text);
    double pAtivoMax = parseNumber(controllerPAtivoMax.text) == 0
        ? 9999999999
        : parseNumber(controllerPAtivoMax.text);

    double pAtivoCirculanteMin = controllerPAtivoCirculanteMin.text == ""
        ? -9999999999
        : parseNumber(controllerPAtivoCirculanteMin.text);
    double pAtivoCirculanteMax =
        parseNumber(controllerPAtivoCirculanteMax.text) == 0
            ? 999999999
            : parseNumber(controllerPAtivoCirculanteMax.text);

    double pCapitalDeGiroMin = controllerPCapitalDeGiroMin.text == ""
        ? -9999999999
        : parseNumber(controllerPCapitalDeGiroMin.text);
    double pCapitalDeGiroMax =
        parseNumber(controllerPCapitalDeGiroMax.text) == 0
            ? 99999999
            : parseNumber(controllerPCapitalDeGiroMax.text);

    double pSRMin = controllerPSRMin.text == ""
        ? -9999999999
        : parseNumber(controllerPSRMin.text);
    double pSRMax = parseNumber(controllerPSRMax.text) == 0
        ? 999999999
        : parseNumber(controllerPSRMax.text);

    double pVPMin = controllerPVPMin.text == ""
        ? -9999999999
        : parseNumber(controllerPVPMin.text);
    double pVPMax = parseNumber(controllerPVPMax.text) == 0
        ? 99999999
        : parseNumber(controllerPVPMax.text);

    double pLAtivoMin = controllerPLAtivoMin.text == ""
        ? -9999999999
        : parseNumber(controllerPLAtivoMin.text);
    double pLAtivoMax = parseNumber(controllerPLAtivoMax.text) == 0
        ? 99999999
        : parseNumber(controllerPLAtivoMax.text);

    double receitasCagr5Min = controllerReceitasCagr5Min.text == ""
        ? -9999999999
        : parseNumber(controllerReceitasCagr5Min.text);
    double receitasCagr5Max = parseNumber(controllerReceitasCagr5Max.text) == 0
        ? 99999999
        : parseNumber(controllerReceitasCagr5Max.text);

    double roaMin = controllerROAMin.text == ""
        ? -9999999999
        : parseNumber(controllerROAMin.text);
    double roaMax = parseNumber(controllerROAMax.text) == 0
        ? 99999999
        : parseNumber(controllerROAMax.text);

    double roeMin = controllerROEMin.text == ""
        ? -9999999999
        : parseNumber(controllerROEMin.text);
    double roeMax = parseNumber(controllerROEMax.text) == 0
        ? 99999999
        : parseNumber(controllerROAMax.text);

    if (totalSharesCopy == null) {
      isFilterEmptyList = true;
      print(
          "numero de ações que correspondem ao filtro: ${searchResult.length}");
      shareBloc.shareController.sink.add([]);
      return;
    } else {
      print(totalSharesCopy.length);
      totalSharesCopy.forEach((share) {
        if (share.margemLiquida >= margemLiquidaMin &&
            share.margemLiquida <= margemLiquidaMax &&
            share.p_L >= plMin &&
            share.p_L <= plMax &&
            share.passivo_Ativo >= passivoAtivoMin &&
            share.passivo_Ativo <= passivoAtivoMax &&
            share.dy >= dyMin &&
            share.dy <= dyMax &&
            share.liquidezCorrente >= liquidezCorrenteMin &&
            share.liquidezCorrente <= liquidezCorrenteMax &&
            share.lucros_Cagr5 >= lucrosCagr5Min &&
            share.lucros_Cagr5 <= lucrosCagr5Max &&
            share.margemBruta >= margemBrutaMin &&
            share.margemBruta <= margemBrutaMax &&
            share.margemEbit >= margemEbitMin &&
            share.margemEbit <= margemEbitMax &&
            share.p_ativo >= pAtivoMin &&
            share.p_ativo <= pAtivoMax &&
            share.p_ativoCirculante >= pAtivoCirculanteMin &&
            share.p_ativoCirculante <= pAtivoCirculanteMax &&
            share.p_capitalGiro >= pCapitalDeGiroMin &&
            share.p_capitalGiro <= pCapitalDeGiroMax &&
            share.p_SR >= pSRMin &&
            share.p_SR <= pSRMax &&
            share.p_VP >= pVPMin &&
            share.p_VP <= pVPMax &&
            share.pl_Ativo >= pLAtivoMin &&
            share.pl_Ativo <= pLAtivoMax &&
            share.receitas_Cagr5 >= receitasCagr5Min &&
            share.receitas_Cagr5 <= receitasCagr5Max &&
            share.roa >= roaMin &&
            share.roa <= roaMax &&
            share.roe >= roeMin &&
            share.roe <= roeMax) {
          print("roeMax: $roeMax");
          print("roeMin:$roeMin");
          print(share.roe);
          searchResult.add(share);
        }
      });
    }
    shareBloc.shareController.sink.add(searchResult);
    print("numero de ações que correspondem ao filtro: ${searchResult.length}");
  }

  Future<void> _showFilterDialog(BuildContext context) async {
    return showDialog<void>(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return AlertDialog(
            contentPadding: EdgeInsets.zero,
            scrollable: true,
            title: Text("Filtros"),
            content: SingleChildScrollView(child: filterWidget(context)),
            actions: <Widget>[
              TextButton(
                child: Text("Aplicar filtros"),
                onPressed: () {
                  filterListByProperties();
                  Navigator.of(context).pop();
                },
              ),
              TextButton(
                child: Text("Limpar Filtros"),
                onPressed: () {
                  print("Salvando filtros");
                },
              ),
              TextButton(
                child: Text("Cancelar"),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              )
            ],
          );
        });
  }

  Widget filterWidget(BuildContext context) {
    double widthOfTextField = 60;
    return Padding(
      padding: const EdgeInsets.only(bottom: 10),
      child: Container(
        height: 400,
        width: MediaQuery.of(context).size.width * 1,
        child: Form(
          child: ListView(
            shrinkWrap: true,
            children: <Widget>[
              Container(
                  child: ExpansionTile(
                title: Text("Margem Líquida"),
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      Text("mínimo: "),
                      Container(
                        width: widthOfTextField,
                        child: TextFormField(
                          controller: controllerMargemLiquidaMin,
                          keyboardType: TextInputType.number,
                        ),
                      ),
                      Text("Máximo"),
                      Container(
                        width: widthOfTextField,
                        child: TextFormField(
                          controller: controllerMargemLiquidaMax,
                          keyboardType: TextInputType.number,
                        ),
                      ),
                    ],
                  )
                ],
              )),
              ExpansionTile(title: Text("P/L"), children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    Text("Mínimo"),
                    Container(
                      width: widthOfTextField,
                      child: TextFormField(
                        controller: controllerPLMin,
                        keyboardType: TextInputType.number,
                      ),
                    ),
                    Text("Máximo"),
                    Container(
                      width: widthOfTextField,
                      child: TextFormField(
                        controller: controllerPLMax,
                        keyboardType: TextInputType.number,
                      ),
                    ),
                  ],
                )
              ]),
              ExpansionTile(
                title: Text("Passivos/Ativos"),
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      Text("Mínimo"),
                      Container(
                        width: widthOfTextField,
                        child: TextFormField(
                          controller: controllerPassivoAtivoMin,
                          keyboardType: TextInputType.number,
                        ),
                      ),
                      Text("Máximo"),
                      Container(
                        width: widthOfTextField,
                        child: TextFormField(
                          controller: controllerPassivoAtivoMax,
                          keyboardType: TextInputType.number,
                        ),
                      ),
                    ],
                  )
                ],
              ),
              ExpansionTile(
                title: Text("DY"),
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      Text("Mínimo"),
                      Container(
                        width: widthOfTextField,
                        child: TextFormField(
                          controller: controllerDYMin,
                          keyboardType: TextInputType.number,
                        ),
                      ),
                      Text("Máximo"),
                      Container(
                        width: widthOfTextField,
                        child: TextFormField(
                          controller: controllerDYMax,
                          keyboardType: TextInputType.number,
                        ),
                      ),
                    ],
                  )
                ],
              ),
              ExpansionTile(
                title: Text("Liquidez Corrente"),
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      Text("Mínimo"),
                      Container(
                        width: widthOfTextField,
                        child: TextFormField(
                          controller: controllerMargemLiquidaMin,
                          keyboardType: TextInputType.number,
                        ),
                      ),
                      Text("Máximo"),
                      Container(
                        width: widthOfTextField,
                        child: TextFormField(
                          controller: controllerMargemLiquidaMax,
                          keyboardType: TextInputType.number,
                        ),
                      )
                    ],
                  )
                ],
              ),
              ExpansionTile(
                title: Text("Lucros CAGR 5 anos"),
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      Text("Mínimo"),
                      Container(
                        width: widthOfTextField,
                        child: TextFormField(
                            keyboardType: TextInputType.number,
                            controller: controllerLucrosCagr5Min),
                      ),
                      Text("Máximo"),
                      Container(
                        width: widthOfTextField,
                        child: TextFormField(
                          controller: controllerLucrosCagr5Max,
                          keyboardType: TextInputType.number,
                        ),
                      )
                    ],
                  )
                ],
              ),
              ExpansionTile(
                title: Text("Margem Bruta"),
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      Text("Mínimo"),
                      Container(
                        width: widthOfTextField,
                        child: TextFormField(
                          controller: controllerMargemBrutaMin,
                          keyboardType: TextInputType.number,
                        ),
                      ),
                      Text("Máximo"),
                      Container(
                        width: widthOfTextField,
                        child: TextFormField(
                          controller: controllerMargemBrutaMax,
                          keyboardType: TextInputType.number,
                        ),
                      )
                    ],
                  )
                ],
              ),
              ExpansionTile(
                title: Text("Margem EBIT"),
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      Text("Mínimo"),
                      Container(
                        width: widthOfTextField,
                        child: TextFormField(
                          controller: controllerMargemEbitMin,
                          keyboardType: TextInputType.number,
                        ),
                      ),
                      Text("Máximo"),
                      Container(
                        width: widthOfTextField,
                        child: TextFormField(
                          controller: controllerMargemEbitMax,
                          keyboardType: TextInputType.number,
                        ),
                      )
                    ],
                  )
                ],
              ),
              ExpansionTile(
                title: Text("P/Ativo"),
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      Text("Mínimo"),
                      Container(
                        width: widthOfTextField,
                        child: TextFormField(
                          controller: controllerPAtivoMin,
                          keyboardType: TextInputType.number,
                        ),
                      ),
                      Text("Máximo"),
                      Container(
                        width: widthOfTextField,
                        child: TextFormField(
                          controller: controllerPAtivoMax,
                          keyboardType: TextInputType.number,
                        ),
                      )
                    ],
                  )
                ],
              ),
              ExpansionTile(
                title: Text("P/Ativo Circulante"),
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      Text("Mínimo"),
                      Container(
                        width: widthOfTextField,
                        child: TextFormField(
                          controller: controllerPAtivoCirculanteMin,
                          keyboardType: TextInputType.number,
                        ),
                      ),
                      Text("Máximo"),
                      Container(
                        width: widthOfTextField,
                        child: TextFormField(
                          controller: controllerPAtivoCirculanteMax,
                          keyboardType: TextInputType.number,
                        ),
                      )
                    ],
                  )
                ],
              ),
              ExpansionTile(
                title: Text("P/Capital de Giro"),
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      Text("Mínimo"),
                      Container(
                        width: widthOfTextField,
                        child: TextFormField(
                          controller: controllerPCapitalDeGiroMin,
                          keyboardType: TextInputType.number,
                        ),
                      ),
                      Text("Máximo"),
                      Container(
                        width: widthOfTextField,
                        child: TextFormField(
                          controller: controllerPCapitalDeGiroMax,
                          keyboardType: TextInputType.number,
                        ),
                      )
                    ],
                  )
                ],
              ),
              ExpansionTile(
                title: Text("P/L"),
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      Text("Mínimo"),
                      Container(
                        width: widthOfTextField,
                        child: TextFormField(
                          controller: controllerPLMin,
                          keyboardType: TextInputType.number,
                        ),
                      ),
                      Text("Máximo"),
                      Container(
                        width: widthOfTextField,
                        child: TextFormField(
                          controller: controllerPLMax,
                          keyboardType: TextInputType.number,
                        ),
                      )
                    ],
                  )
                ],
              ),
              ExpansionTile(
                title: Text("P/SR"),
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      Text("Mínimo"),
                      Container(
                        width: widthOfTextField,
                        child: TextFormField(
                          controller: controllerPSRMin,
                          keyboardType: TextInputType.number,
                        ),
                      ),
                      Text("Máximo"),
                      Container(
                        width: widthOfTextField,
                        child: TextFormField(
                            controller: controllerPSRMax,
                            keyboardType: TextInputType.number),
                      )
                    ],
                  )
                ],
              ),
              ExpansionTile(
                title: Text("P/VP"),
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      Text("Mínimo"),
                      Container(
                        width: widthOfTextField,
                        child: TextFormField(
                          controller: controllerPVPMin,
                          keyboardType: TextInputType.number,
                        ),
                      ),
                      Text("Máximo"),
                      Container(
                        width: widthOfTextField,
                        child: TextFormField(
                          controller: controllerPVPMax,
                          keyboardType: TextInputType.number,
                        ),
                      )
                    ],
                  )
                ],
              ),
              ExpansionTile(
                title: Text("PL/Ativo"),
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      Text("Mínimo"),
                      Container(
                        width: widthOfTextField,
                        child: TextFormField(
                          controller: controllerPLAtivoMin,
                          keyboardType: TextInputType.number,
                        ),
                      ),
                      Text("Máximo"),
                      Container(
                        width: widthOfTextField,
                        child: TextFormField(
                          controller: controllerPLAtivoMax,
                          keyboardType: TextInputType.number,
                        ),
                      ),
                    ],
                  )
                ],
              ),
              ExpansionTile(
                title: Text("Receitas CAGR 5 anos"),
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      Text("Mínimo"),
                      Container(
                        width: widthOfTextField,
                        child: TextFormField(
                          controller: controllerReceitasCagr5Min,
                          keyboardType: TextInputType.number,
                        ),
                      ),
                      Text("Máximo"),
                      Container(
                        width: widthOfTextField,
                        child: TextFormField(
                          controller: controllerReceitasCagr5Max,
                          keyboardType: TextInputType.number,
                        ),
                      )
                    ],
                  )
                ],
              ),
              ExpansionTile(
                title: Text("ROA"),
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      Text("Mínimo"),
                      Container(
                        width: widthOfTextField,
                        child: TextFormField(
                          controller: controllerROAMin,
                          keyboardType: TextInputType.number,
                        ),
                      ),
                      Text("Máximo"),
                      Container(
                        width: widthOfTextField,
                        child: TextFormField(
                          controller: controllerROAMax,
                          keyboardType: TextInputType.number,
                        ),
                      )
                    ],
                  )
                ],
              ),
              ExpansionTile(
                title: Text("ROE"),
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      Text("Mínimo"),
                      Container(
                        width: widthOfTextField,
                        child: TextFormField(
                          controller: controllerROEMin,
                          keyboardType: TextInputType.number,
                        ),
                      ),
                      Text("Máximo"),
                      Container(
                        width: widthOfTextField,
                        child: TextFormField(
                          controller: controllerROEMax,
                          keyboardType: TextInputType.number,
                        ),
                      )
                    ],
                  )
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }

  final int teste = 2;
  @override
  Widget build(BuildContext context) {
    shareBloc.shareController.stream.listen((data) {
      totalShares = data;
      print('total shares updated');
    });
    return teste == 1
        ? BackdropScaffold(
            stickyFrontLayer: true,
            iconPosition: BackdropIconPosition.leading,
            actions: <Widget>[
              BackdropToggleButton(icon: AnimatedIcons.search_ellipsis)
            ],
            title: Text('Bolter'),
            backLayer: Container(
              child: BackdropNavigationBackLayer(items: [
                ListTile(
                  title: Text(
                    "Filtrar",
                  ),
                  trailing: Icon(Icons.search),
                  onTap: () {
                    _showFilterDialog(context);
                  },
                ),
                ListTile(
                  title: Text("Sair"),
                  trailing: Icon(Icons.logout),
                  onTap: () {
                    print("hello world");
                  },
                ),
              ]),
            ),
            frontLayer: Container(
                child: Column(
              children: <Widget>[
                SizedBox(
                  height: 10,
                ),
                searchBox(),
                SizedBox(
                  height: 20,
                ),
                Expanded(child: shareList())
              ],
            )),
          )
        : Scaffold(
            drawer: Drawer(
              child: ListView(
                padding: EdgeInsets.zero,
                children: [
                  DrawerHeader(
                    child: Text("Menu de Opções"),
                    decoration:
                        BoxDecoration(color: Theme.of(context).primaryColor),
                  ),
                  ListTile(
                    title: Text("Filtrar"),
                    trailing: Icon(Icons.search),
                    onTap: () {
                      Navigator.pop(context);
                      _showFilterDialog(context);
                    },
                  ),
                  ListTile(
                    title: Text("Sair"),
                    trailing: Icon(Icons.logout),
                    onTap: () {
                      BolterAPI.deleteFileLogout();
                      Navigator.pushNamedAndRemoveUntil(
                          context, '/welcomeRoute', (route) => false);
                    },
                  )
                ],
              ),
            ),
            appBar: AppBar(
              title: Text("Bolter"),
            ),
            body: Container(
                child: Column(
              children: <Widget>[
                SizedBox(
                  height: 10,
                ),
                searchBox(),
                SizedBox(
                  height: 20,
                ),
                Expanded(child: shareList())
              ],
            )),
          );
  }
}

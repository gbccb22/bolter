import 'dart:async';

import 'package:bolter_app/bolterAPI/BolterAPI.dart';
import 'package:bolter_app/tools/ConnectionStatusSingleton.dart';
import 'package:bolter_app/tools/ShareBloc.dart';
import 'package:email_validator/email_validator.dart';
import 'package:flutter/material.dart';
import 'package:string_validator/string_validator.dart' as sv;

class RegisterRoute extends StatefulWidget {
  @override
  _RegisterRouteState createState() => _RegisterRouteState();
}

class _RegisterRouteState extends State<RegisterRoute> {
  StreamSubscription _connectionChangeStream;
  final GlobalKey<ScaffoldState> _scaffoldState =
      new GlobalKey<ScaffoldState>();
  final _formKey = GlobalKey<FormState>();
  final _textFieldState = GlobalKey();
  String _password1;
  String _password2;
  String _email;
  String _username;
  bool _couldRegister = true;

  @override
  void initState() {
    super.initState();

    ConnectionStatusSingleton connectionStatus =
        ConnectionStatusSingleton.getInstance();

    _connectionChangeStream =
        connectionStatus.connectionChange.listen(connectionChanged);
  }

  bool isOffline = false;

  void connectionChanged(dynamic hasConnection) {
    setState(() {
      isOffline = !hasConnection;
    });
  }

  Widget registerForm(BuildContext context) {
    return Container(
      child: Center(
        child: Form(
          key: _formKey,
          child: ListView(
            children: <Widget>[
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Container(
                    child: Image(image: AssetImage('assets/bolter_logo.png')),
                  ),
                  TextFormField(
                    key: _textFieldState,
                    validator: (String value) {
                      if (EmailValidator.validate(value) == false) {
                        return "email não é valido";
                      }
                      if (_couldRegister == false) {
                        return "Já existe um usário com esse email";
                      }

                      return null;
                    },
                    onSaved: (value) => _email = value.trim(),
                    decoration: InputDecoration(labelText: "email"),
                    keyboardType: TextInputType.emailAddress,
                    style: TextStyle(color: Colors.black),
                  ),
                  SizedBox(height: 20),
                  TextFormField(
                    validator: (String value) {
                      if (value.length == 0) {
                        return "Este campo não pode ficar vazio!";
                      }
                      return null;
                    },
                    onSaved: (value) => _username = value.trim(),
                    decoration: InputDecoration(
                        labelText: "Como gostaria de ser chamado?"),
                    keyboardType: TextInputType.text,
                    style: TextStyle(color: Colors.black),
                  ),
                  SizedBox(height: 20),
                  TextFormField(
                    validator: (String value) {
                      if (value.length < 8) {
                        return "A senha deve ter 8 ou mais caracteres.";
                      }
                      if (sv.isAlphanumeric(value) == false) {
                        return "A senha deve deve conter apenas letras e números";
                      }

                      return null;
                    },
                    style: TextStyle(color: Colors.black),
                    obscureText: true,
                    onSaved: (value) => _password1 = value.trim(),
                    decoration: InputDecoration(labelText: "Senha"),
                  ),
                  SizedBox(height: 20),
                  TextFormField(
                    validator: (String value) {
                      if (value != _password1) {
                        return "As senhas não são iguais!";
                      }
                      return null;
                    },
                    style: TextStyle(color: Colors.black),
                    obscureText: true,
                    onSaved: (value) => _password2 = value.trim(),
                    decoration: InputDecoration(labelText: "Repita sua senha"),
                  ),
                  SizedBox(height: 20),
                  RaisedButton(
                    elevation: 4,
                    child: Text("ENVIAR"),
                    onPressed: isOffline
                        ? null
                        : () async {
                            final form = _formKey.currentState;
                            form.save();

                            if (form.validate()) {
                              print(
                                  "$_email $_username $_password1 $_password2");
                              var response = await BolterAPI.register(
                                  _username, _email, _password1, _password2);

                              if (response == true) {
                                await shareBloc.getShares();
                                Navigator.of(context).popAndPushNamed('/');
                              } else {
                                _textFieldState.currentState.setState(() {
                                  _couldRegister = false;
                                });
                              }
                            }
                          },
                  )
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(), key: _scaffoldState, body: registerForm(context));
  }
}

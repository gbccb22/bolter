import 'dart:async';

import 'package:bolter_app/bolterAPI/BolterAPI.dart';
import 'package:bolter_app/tools/ConnectionStatusSingleton.dart';
import 'package:bolter_app/tools/ShareBloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class LoginRoute extends StatefulWidget {
  @override
  _LoginRouteState createState() => _LoginRouteState();
}

class _LoginRouteState extends State<LoginRoute> {
  StreamSubscription _connectionChangeStream;

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    ConnectionStatusSingleton connectionStatus =
        ConnectionStatusSingleton.getInstance();

    _connectionChangeStream =
        connectionStatus.connectionChange.listen(connectionChanged);
  }

  bool isOffline = false;

  void connectionChanged(dynamic hasConnection) {
    setState(() {
      isOffline = !hasConnection;
    });
  }

  @override
  Widget build(BuildContext context) {
    final _scaffoldKey = GlobalKey<ScaffoldState>();
    final _formKey = GlobalKey<FormState>();
    final _buttonKey = GlobalKey();
    bool _isEnabled = true;
    String _password;
    String _email;

    return Scaffold(
      appBar: AppBar(),
      key: _scaffoldKey,
      body: isOffline
          ? Center(
              child: Text("Pai ta off"),
            )
          : Container(
              child: Center(
                child: Form(
                  key: _formKey,
                  child: ListView(
                    children: <Widget>[
                      Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Container(
                              child: Image(
                                  image: AssetImage('assets/bolter_logo.png'))),
                          TextFormField(
                            style: TextStyle(color: Colors.black),
                            onSaved: (value) => _email = value,
                            keyboardType: TextInputType.emailAddress,
                            decoration: InputDecoration(labelText: "email"),
                          ),
                          SizedBox(
                            height: 40,
                          ),
                          TextFormField(
                            style: TextStyle(color: Colors.black),
                            onSaved: (value) => _password = value,
                            obscureText: true,
                            decoration: InputDecoration(labelText: "Senha"),
                          ),
                          SizedBox(
                            height: 40,
                          ),
                          SizedBox(
                            height: 20,
                          ),
                          RaisedButton(
                            key: _buttonKey,
                            elevation: 4,
                            child: Text("LOGIN"),
                            onPressed: _isEnabled
                                ? () async {
                                    final form = _formKey.currentState;
                                    form.save();

                                    if (form.validate()) {
                                      var response = await BolterAPI.login(
                                          _email, _password);

                                      if (response == true) {
                                        await shareBloc.getShares();
                                        Navigator.of(context)
                                            .pushNamedAndRemoveUntil(
                                                '/', (r) => false);
                                      } else {
                                        _scaffoldKey.currentState.showSnackBar(
                                            SnackBar(
                                                content: Text(
                                                    "Email ou senha inválidos")));
                                      }
                                    }
                                  }
                                : null,
                          )
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ),
    );
  }
}

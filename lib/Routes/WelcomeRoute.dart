import 'package:flutter/material.dart';


void main() => runApp(WelcomeRoute());

class WelcomeRoute extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
        child: SafeArea(
          child: Scaffold(
            body: Container(
                child: Center(
              child: Container(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      child: Image(
                        image: AssetImage('assets/bolter_logo.png'),
                      ),
                    ),
                    Container(
                      decoration: BoxDecoration(
                          color: Colors.grey[800],
                          border: Border.all(color: Colors.amber[700]),
                          borderRadius: BorderRadius.circular(50)),
                      width: 260,
                      height: 120,
                      child: Center(
                          child: Text(
                        'descubra seu perfil de investidor, experimente diversos recursos gratuitos e conquiste sua liberdade financeira'
                            .toUpperCase(),
                        style: TextStyle(color: Colors.white, height: 2),
                        textAlign: TextAlign.center,
                      )),
                    ),
                    SizedBox(height: 20,),
                    Container(
                      width: 260,
                      height: 50,
                      child: RaisedButton(
                        elevation: 4,
                        onPressed: () {
                          Navigator.pushNamed(context, '/loginRoute');
                        },
                        child: Text(
                          'Fazer login'.toUpperCase(),
                          style: TextStyle(color: Colors.white),
                        ),
                        color: Colors.amber[700],
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(5),
                            side: BorderSide(color: Colors.white)),
                      ),
                    ),
                    SizedBox(height: 20,),
                    Container(
                      width: 260,
                      height: 50,
                      child: RaisedButton(
                        onPressed: () {
                          Navigator.pushNamed(context, '/registerRoute');
                        },
                        elevation: 4,
                        color: Colors.black,
                        textColor: Colors.white,
                        child: Text('Registre-se'.toUpperCase()),
                        shape: RoundedRectangleBorder(
                            side: BorderSide(color: Colors.white),
                            borderRadius: BorderRadius.circular(5)),
                      ),
                    ),
                  ],
                ),
              ),
            )),
          ),
        ),
      );
  }
}

Widget welcomeWdiget() {
  return Container(
    child: Column(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[Text('BOLTER')],
    ),
  );
}

import 'package:bolter_app/Routes/LoginRoute.dart';
import 'package:bolter_app/Routes/RegisterRoute.dart';
import 'package:bolter_app/Routes/ShareDetailsRoute.dart';
import 'package:bolter_app/Routes/WelcomeRoute.dart';
import 'package:bolter_app/Routes/mainRoute.dart';
import 'package:bolter_app/tools/ConnectionStatusSingleton.dart';
import 'package:flutter/material.dart';
import 'package:path_provider/path_provider.dart' as path_provider;
import 'dart:io' as io;

Future<bool> _fileExists() async {
  final directory = await path_provider.getTemporaryDirectory();
  String _localPath = directory.path;
  if (await io.File("$_localPath/key.txt").exists() == true) {
    return true;
  }
  return false;
}

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  bool _isLoggedIn = await _fileExists();

  print("O usuário está logado? " + "$_isLoggedIn");
  final MyApp myApp = MyApp(
    initialRoute: _isLoggedIn ? '/' : '/welcomeRoute',
  );

  ConnectionStatusSingleton connectionStatus =
      ConnectionStatusSingleton.getInstance();
  connectionStatus.initialize();

  runApp(myApp);
}

class MyApp extends StatelessWidget {
  final String initialRoute;

  MyApp({this.initialRoute});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      routes: {
        '/welcomeRoute': (context) => WelcomeRoute(),
        '/': (context) => MainRoute(),
        '/loginRoute': (context) => LoginRoute(),
        '/registerRoute': (context) => RegisterRoute(),
        '/shareDetailsRoute': (context) => ShareDetalisRoute()
      },
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
          primaryColor: Colors.amber[700],
          accentColor: Colors.black,
          primaryIconTheme: IconThemeData(color: Colors.black),
          textTheme: TextTheme(
              subhead: TextStyle(fontSize: 15, color: Colors.black),
              body1: TextStyle(
                fontSize: 13,
              ),
              body2: TextStyle(fontSize: 20))),
      title: 'Bolter',
      initialRoute: initialRoute,
    );
  }
}
